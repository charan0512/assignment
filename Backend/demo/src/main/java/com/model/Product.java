package com.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Id;
@Entity

public class Product 
{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String name;
	private String weight ;
	
	@Lob
	private byte[] image;
	private Long price;
	
	
	public Product()
	{
		
	}

	public Product(String name, String weight,byte[] image,
			Long price)
	{
		this.name= name;
		this.weight = weight;
		this.image = image;
		this.price = price;
	}


	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public String getweight() {
		return weight;
	}

	public void setweight(String weight) {
		this.weight = weight;
	}


	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

}