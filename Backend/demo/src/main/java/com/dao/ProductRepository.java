package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Product;

@Repository

public interface ProductRepository extends JpaRepository<Product, Long> {
    Product findByName(String name);

    @Query("from Product p where p.price = :price")
    Product findByPrice(@Param("price") Long price);

    @Query("from Product p where p.weight = :weight")
    List<Product> findByWeight(@Param("weight") String weight);
}
